let startMasive = ["Джаз", "Блюз"];
document.write("Створено початковий масив: " + startMasive);
document.write("<br>");
startMasive.push("Рок-н-рол");
document.write("Додано ще один елемент: " + startMasive);
document.write("<br>");
let i = startMasive.length/2;
let k = parseInt(i);
startMasive[k]= "Класика";
document.write("Заміна середнього значення: " + startMasive);
document.write("<br>");
let element = startMasive.slice([0],[1]);
document.write("Видалений елемент: " + element);
document.write("<br>");
startMasive.unshift("Реп", "Регі");
document.write("Додано 2 елемента на початок: " + startMasive);
document.write("<hr>");
//Додаткові задачки
// 1. Даны два массива: ['a', 'b', 'c'] и [1, 2, 3]. Объедините их вместе.
let firstMasive = ['a', 'b', 'c'];
let secondMasive = [1, 2, 3];
let finalMasive = firstMasive.concat(secondMasive);
document.write(finalMasive);
document.write("<br>");
//2. Дан массив ['a', 'b', 'c']. Добавьте ему в конец элементы 1, 2, 3.
let firstMasive1 = ['a', 'b', 'c'];
firstMasive1.push(1, 2, 3);
document.write(firstMasive1);
document.write("<br>");
//3. Дан массив [1, 2, 3]. Сделайте из него массив [3, 2, 1].
let firstMasive2 = [1, 2, 3];
firstMasive2.reverse();
document.write(firstMasive2);
document.write("<br>");
//4.Дан массив [1, 2, 3]. Добавьте ему в конец элементы 4, 5, 6.
let firstMasive3 = [1, 2, 3];
firstMasive3.push(4, 5, 6);
document.write(firstMasive3);
document.write("<br>");
// 5.Дан массив [1, 2, 3]. Добавьте ему в начало элементы 4, 5, 6.
let firstMasive4 = [1, 2, 3];
firstMasive4.unshift(4, 5, 6);
document.write(firstMasive4);
document.write("<br>");
// 6. Дан массив ['js', 'css', 'jq']. Выведите на экран первый элемент.
let jsMasive = ['js', 'css', 'jq'];
document.write(jsMasive[0]);
document.write("<br>");
//7. Дан массив [1, 2, 3, 4, 5]. С помощью метода slice запишите в новый элементы [1, 2, 3].
let firstMasive5 = [1, 2, 3, 4, 5];
let secondMasive1 = firstMasive5.slice(0,3);
document.write(secondMasive1);
document.write("<br>");
// 8. Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 4, 5].
let firstMasive6 = [1, 2, 3, 4, 5]
firstMasive6.splice(1,2);
document.write(firstMasive6);
document.write("<br>");
//9.Дан массив [1, 2, 3, 4, 5]. С помощью метода splice преобразуйте массив в [1, 2, 10, 3, 4, 5].
let firstMasive7 = [1, 2, 3, 4, 5];
firstMasive7.splice(2,0,10);
document.write(firstMasive7);
document.write("<br>");
//10. Дан массив [3, 4, 1, 2, 7]. Отсортируйте его.
let firstMasive8 = [3, 4, 1, 2, 7];
firstMasive8.sort();
document.write(firstMasive8);
document.write("<br>");
//11. Дан массив с элементами 'Привет, ', 'мир' и '!'. Необходимо вывести на экран фразу 'Привет, мир!'.
let wordMasive = ["Привет, ", "мир", "!"];
document.write(wordMasive.join(""));
document.write("<br>");
// 12. Дан массив ['Привет, ', 'мир', '!']. Необходимо записать в нулевой элемент этого массива слово 'Пока, ' (то есть вместо слова 'Привет, ' будет 'Пока, ').\
let wordMasive1 = ['Привет, ', 'мир', '!'];
wordMasive1[0] = ("Пока, ");
document.write(wordMasive1.join(""));
document.write("<br>");
// 13. Создайте массив arr с элементами 1, 2, 3, 4, 5 двумя различными способами.
let newMassive = [1, 2, 3, 4, 5];
let newMassive1 = new Array(1, 2, 3, 4, 5);
/* 14.Дан многомерный массив arr:  Выведите с его помощью слово 'голубой' 'blue' .*/
var arr = {
 'ru':['голубой', 'красный', 'зеленый'],
 'en':['blue', 'red', 'green'],
 };
document.write(arr.ru[0], arr.en[0]);
document.write("<br>");
// 15. Создайте массив arr = ['a', 'b', 'c', 'd'] и с его помощью выведите на экран строку 'a+b, c+d'. 
var arr1 = ['a', 'b', 'c', 'd'];
document.write(arr1[0] + "+" + arr1[1] + " , " + arr1[2]+ "+" + arr1[3]);
document.write("<br>");
//16. Запросите у пользователя количество элементов массива. Исходя из данных которые ввел пользователь создайте массив на то количество элементов которое передал пользователь. в кажlом индексе массива храните чило которе будет показывать номер элемента массива. 
let n = prompt("Вкажіть число - кількість елементів масиву");
let m = parseInt(n);
let userMasive = new Array(m);
let t = 0;
while(t<m){
	userMasive[t] = t;
	t++;
};
document.write(userMasive);
document.write("<br>");
// 17. Сделайте так, чтобы из массива который вы создали выше вывелись все нечетные числа в параграфе, а четные в спане с красным фоном.
let b = 0;
while(b<m){
	userMasive[b] = b;
	if(b%2 === 0){
		userMasive[b] = ("<p>" + b + "</p>");
	} else {
		userMasive[b] = ("<span style = 'background-color: red'>" + b + "</span>");
	}
	b++;
};
document.write(userMasive);
document.write("<br>");
// 18. Напишите код, который преобразовывает и объединяет все элементы массива в одно строковое значение. Элементы массива будут разделены запятой.
var vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
document.write(vegetables.join(", "));



